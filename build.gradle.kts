import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.6.10"
}

group = "me.vorrawut"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    // These dependencies are used by the application.
    implementation("io.temporal:temporal-sdk:1.3.1")
    implementation("com.google.guava:guava:30.1.1-jre")
    implementation("ch.qos.logback:logback-classic:1.2.6")

    testImplementation(kotlin("test"))
    testImplementation("org.mockito:mockito-core:4.2.0")
    testImplementation("io.temporal:temporal-testing:1.5.0")
    testImplementation("io.temporal:temporal-testing-junit4:1.5.0")
    testImplementation("junit:junit:4.13.2")
}

tasks.test {
    useJUnit()
}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "11"
}
