package com.helloworld.app.workflow;

import com.helloworld.app.activity.Format
import io.temporal.activity.ActivityOptions
import io.temporal.workflow.Workflow
import java.time.Duration


class GreetingWorkflowImpl: GreetingWorkflow {

    private var activityOptions = ActivityOptions.newBuilder().setScheduleToCloseTimeout(Duration.ofSeconds(2))
        .build();

    private var format = Workflow.newActivityStub(Format::class.java, activityOptions)

    override fun getGreeting(name: String): String {
        return format.composeGreeting(name)
    }
}