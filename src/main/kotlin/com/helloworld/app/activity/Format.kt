package com.helloworld.app.activity;

import io.temporal.activity.ActivityInterface;
import io.temporal.activity.ActivityMethod;

@ActivityInterface
interface Format {

    @ActivityMethod
    fun composeGreeting(name: String): String
}