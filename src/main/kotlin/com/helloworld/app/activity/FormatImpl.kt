package com.helloworld.app.activity;

class FormatImpl : Format {
    override fun composeGreeting(name: String): String {
        return "Hello $name!"
    }
}