package com.helloworld.app.shared

interface Shared {
    companion object {
        const val HELLO_WORLD_TASK_QUEUE = "HELLO_WORLD_TASK_QUEUE"
    }
}