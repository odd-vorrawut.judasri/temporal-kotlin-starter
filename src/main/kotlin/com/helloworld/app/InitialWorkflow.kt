package com.helloworld.app

import com.helloworld.app.shared.Shared
import com.helloworld.app.workflow.GreetingWorkflow
import io.temporal.client.WorkflowClient
import io.temporal.client.WorkflowOptions
import io.temporal.serviceclient.WorkflowServiceStubs

object InitiateWorkflow {
    @Throws(Exception::class)
    @JvmStatic
    fun main(args: Array<String>) {
        // This gRPC stubs wrapper talks to the local docker instance of the Temporal service.
        val service = WorkflowServiceStubs.newInstance()
        // WorkflowClient can be used to start, signal, query, cancel, and terminate Workflows.
        val client = WorkflowClient.newInstance(service)
        val options = WorkflowOptions.newBuilder()
            .setTaskQueue(Shared.HELLO_WORLD_TASK_QUEUE)
            .build()
        // WorkflowStubs enable calls to methods as if the Workflow object is local, but actually perform an RPC.
        val workflow = client.newWorkflowStub(GreetingWorkflow::class.java, options)
        // Synchronously execute the Workflow and wait for the response.
        val greeting: String = workflow.getGreeting("World")
        println(greeting)
        System.exit(0)
    }
}