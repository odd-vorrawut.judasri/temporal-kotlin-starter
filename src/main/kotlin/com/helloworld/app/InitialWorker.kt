package com.helloworld.app

import com.helloworld.app.activity.FormatImpl
import com.helloworld.app.shared.Shared
import com.helloworld.app.workflow.GreetingWorkflowImpl
import io.temporal.client.WorkflowClient
import io.temporal.serviceclient.WorkflowServiceStubs
import io.temporal.worker.Worker
import io.temporal.worker.WorkerFactory


object InitialWorker {
    @JvmStatic
    fun main(args: Array<String>) {
        // This gRPC stubs wrapper talks to the local docker instance of the Temporal service.
        val service = WorkflowServiceStubs.newInstance()
        val client = WorkflowClient.newInstance(service)
        // Create a Worker factory that can be used to create Workers that poll specific Task Queues.
        val factory = WorkerFactory.newInstance(client)
        val worker: Worker = factory.newWorker(Shared.HELLO_WORLD_TASK_QUEUE)
        // This Worker hosts both Workflow and Activity implementations.
        // Workflows are stateful, so you need to supply a type to create instances.
        worker.registerWorkflowImplementationTypes(GreetingWorkflowImpl::class.java)
        // Activities are stateless and thread safe, so a shared instance is used.
        worker.registerActivitiesImplementations(FormatImpl())
        // Start polling the Task Queue.
        factory.start()
    }
}