package com.helloworld.app

import com.helloworld.app.activity.Format
import com.helloworld.app.activity.FormatImpl
import com.helloworld.app.workflow.GreetingWorkflow
import com.helloworld.app.workflow.GreetingWorkflowImpl
import io.temporal.client.WorkflowOptions

import io.temporal.testing.TestWorkflowRule
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.*

class InitialWorkFlowTest {

    @Rule
    @JvmField
    var testWorkflowRule: TestWorkflowRule = TestWorkflowRule.newBuilder()
        .setWorkflowTypes(GreetingWorkflowImpl::class.java)
        .setDoNotStart(true)
        .build()

    @Test
    fun testGetGreeting() {
        testWorkflowRule.worker.registerActivitiesImplementations(FormatImpl())
        testWorkflowRule.testEnvironment.start()
        val workflow: GreetingWorkflow = testWorkflowRule
            .workflowClient
            .newWorkflowStub(
                GreetingWorkflow::class.java,
                WorkflowOptions.newBuilder().setTaskQueue(testWorkflowRule.taskQueue).build()
            )
        val greeting: String = workflow.getGreeting("John")
        Assert.assertEquals("Hello John!", greeting)
        testWorkflowRule.testEnvironment.shutdown()
    }

    @Test
    fun testMockedGetGreeting() {
        val formatActivities: Format = mock(Format::class.java, withSettings().withoutAnnotations())
        `when`(formatActivities.composeGreeting(anyString())).thenReturn("Hello World!")
        testWorkflowRule.worker.registerActivitiesImplementations(formatActivities)
        testWorkflowRule.testEnvironment.start()
        val workflow: GreetingWorkflow = testWorkflowRule
            .workflowClient
            .newWorkflowStub(
                GreetingWorkflow::class.java,
                WorkflowOptions.newBuilder().setTaskQueue(testWorkflowRule.taskQueue).build()
            )
        val greeting: String = workflow.getGreeting("World")
        Assert.assertEquals("Hello World!", greeting)
        testWorkflowRule.testEnvironment.shutdown()
    }
}